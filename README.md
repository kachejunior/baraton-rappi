# El Baratón

Este proyecto fue generado a partir de [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.
Se requiere:
*nodejs preferiblemente en su última versión
*angular-cli preferiblemente en su última versión

## Instalación

Para ejecutar en modo desarrollo primero instalar dependencias `npm install` y luego ejecutar `npm start` se podrá ver en cualquier navegador ingresando a  `http://localhost:4200/`, para modo produccíón luego de instalar las dependecias con  `npm install` se procede a ejecutar `ng build --prod` y creará una carpeta 
`/dist` ideal para agregar en un servidor Apache o Ngix.

## Algunas consideraciones
*En base al material facilitado (categories.json y products.json) en la lista de productos modifique el formato del atributo `price` a tipo number (anteriormente me entregaron un string con el simbolo de $ y formato para imprimir) esto fue necesario para poder procesar en el carrito las operaciones matemáticas.
*En teoria en el archivo `src/app/services/query.services.ts` de cambiarse la url por la de una api que cumpla con el formato de JSON facilitado se debera ejecutar la aplicación.
*Como no hice backend o api, el manejo de los filtros fue a nivel local.
