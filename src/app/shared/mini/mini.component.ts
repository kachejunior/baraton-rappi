/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component, Input, OnInit} from '@angular/core';

import {HelpersService} from '../../core';

@Component({
  selector: 'rp-mini',
  templateUrl: 'mini.component.html',
  styleUrls: ['mini.component.scss']
})

export class MiniComponent implements OnInit {
  @Input() itemInput: any; //Ingreso desde componente padre
  cantidad: number = 1; //Cantidad
  item = {
    quantity: 0,
    price: 0,
    available: true,
    sublevel_id: 0,
    name: '',
    id: ''
  }; //Base del producto

  constructor(private helpersService: HelpersService) {
  }

  /**
   * Inicialización cuando llega de componente padre
   */
  ngOnInit() {
    this.item = this.itemInput;
  }

  /**
   * Validacion de cantidad
   */
  validationNumber(): void {
    if (this.cantidad <= 0) {
      this.cantidad = 1;
    }
    if ((this.cantidad % 1) > 0) {
      this.cantidad = this.cantidad - (this.cantidad % 1);
    }
    if (this.cantidad > this.item.quantity) {
      this.cantidad = this.item.quantity;
    }
  }

  /**
   * Agregar 1 mas
   */
  add() {
    if (this.cantidad < this.item.quantity) {
      this.cantidad++;
    }
    else {
      this.helpersService.errorMsg('Máximo permitido: ' + this.item.quantity);
    }
  }

  /**
   * Descartar 1 mas
   */
  sub() {
    if (this.cantidad > 1) {
      this.cantidad--;
    }
    else {
      this.helpersService.errorMsg('Se necesita al menos 1 producto para ser agregado al carrito');
    }
  }

  /**
   * Agregar a carrito
   */
  addToCart() {
    if (localStorage.getItem('carrito')) {
      let list = JSON.parse(localStorage.getItem('carrito'));
      let control = false;
      for (let i = 0; i < list.length; i++) {
        if (list[i].id == this.item.id) {
          list[i].cantidad = list[i].cantidad + this.cantidad;
          if (list[i].cantidad > list[i].item.quantity) {
            this.helpersService.errorMsg('Se permitirá para este producto hasta: ' + this.item.quantity);
            list[i].cantidad = list[i].item.quantity;
          }
          control = true;
        }
      }
      if (!control) {
        list.push({
          id: this.item.id,
          item: this.item,
          cantidad: this.cantidad
        });
      }
      localStorage.setItem('carrito', JSON.stringify(list));
    }
    else {
      let list = [];
      list.push({
        id: this.item.id,
        item: this.item,
        cantidad: this.cantidad
      });
      localStorage.setItem('carrito', JSON.stringify(list));
    }
    this.helpersService.success('Agregado con éxito');
  }

}
