/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from '@angular/core';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'rp-menu',
  templateUrl: 'menu.component.html',
  styleUrls: ['menu.component.scss']
})

export class MenuComponent {
  count: number = 0;

  constructor() {
    /**
     * Generalmente uso eventos pero como no me estaba funcionando y no tenia suficiente tiempo coloque un intervalo que evalua cada segundo el carrito actual.
     */
    Observable.interval(1000)
      .subscribe(() => {
        if (localStorage.getItem('carrito')) {
          let list: any = JSON.parse(localStorage.getItem('carrito'));
          this.count = 0;
          for (let i = 0; i < list.length; i++) {
            this.count = this.count + parseInt(list[i].cantidad);
          }
        }
        else {
          localStorage.setItem('carrito', JSON.stringify([]));
          this.count = 0;
        }
      });
  }

}
