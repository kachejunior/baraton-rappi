import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

import {ToastrService} from 'ngx-toastr';


@Injectable()
export class HelpersService {

  constructor(private router: Router, private toastrService: ToastrService) {
  }


  /**
   * Notificaciones de mensajes de error
   * @param {string} msg
   */
  public errorMsg(msg: string) {
    this.toastrService.error(msg);
  }

  /**
   * Notificaciones de mensajes de exito
   * @param {string} msg
   */
  public success(msg: string) {
    this.toastrService.success(msg);
  }

}
