/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from '@angular/core';

import {QueryService, HelpersService} from '../../core';

import * as _ from 'lodash';

@Component({
  selector: 'rp-cart',
  templateUrl: 'cart.component.html',
  styleUrls: ['cart.component.scss']
})

export class CartComponent {

  total: number = 0; //Total Final
  listProducts: any = []; //Listado de productos


  constructor(private  queryService: QueryService, private helpersService: HelpersService) {
    if (localStorage.getItem('carrito')) {
      this.listProducts = JSON.parse(localStorage.getItem('carrito'));
      this.updateTotal();
    }
  }

  /**
   * Actualizacion del total final, carrito y validacion de montos.
   */
  updateTotal(): void {
    this.total = 0;
    for (let i = 0; i < this.listProducts.length; i++) {
      if (this.listProducts[i].cantidad > this.listProducts[i].item.quantity) {
        this.listProducts[i].cantidad = this.listProducts[i].item.quantity;
      }
      if (this.listProducts[i].cantidad < 1) {
        if (this.listProducts[i].item.quantity == 0) {
          this.listProducts[i].cantidad = 0;
        }
        else {
          this.listProducts[i].cantidad = 1;
        }
      }
      this.total = this.total + (this.listProducts[i].cantidad * this.listProducts[i].item.price);
    }
    localStorage.setItem('carrito', JSON.stringify(this.listProducts));
  }

  /**
   * Agregar 1 producto mas y actualizar carrito
   * @param producto
   */
  add(producto) {
    if (producto.cantidad < producto.item.quantity) {
      producto.cantidad = parseInt(producto.cantidad) + 1;
      localStorage.setItem('carrito', JSON.stringify(this.listProducts));
      this.updateTotal();
    }
    else {
      this.helpersService.errorMsg('Maximo permitido: ' + producto.item.quantity);
    }
  };

  /**
   * Eliminar 1 producto y actualizar carrito
   * @param producto
   */
  sub(producto) {
    if (producto.cantidad > 1) {
      producto.cantidad = parseInt(producto.cantidad) - 1;
      localStorage.setItem('carrito', JSON.stringify(this.listProducts));
      this.updateTotal();
    }
    else {
      this.helpersService.errorMsg('Se necesita al menos 1 producto para estar en el carrito');
    }
  };

  /**
   * Eliminar producto del carrito
   * @param producto
   */
  removerItem(producto) {
    _.remove(this.listProducts, {id: producto.item.id});
    localStorage.setItem('carrito', JSON.stringify(this.listProducts));
    this.updateTotal();
  };


  /**
   * Proceder en la compra
   */
  procederCompra() {
    this.listProducts.length = 0;
    this.updateTotal();
    localStorage.setItem('carrito', JSON.stringify([]));
    this.helpersService.success('Gracias por su compra');
  }


}
