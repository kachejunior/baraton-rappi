/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from '@angular/core';

import {QueryService, HelpersService} from '../../core';

import * as _ from 'lodash';

@Component({
  selector: 'rp-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss']
})

export class HomeComponent {

  listProducts: any = []; //Listado de productos base
  orderBy: string = '1'; //Tipo de ordenamiento
  search: string = ''; //Buscador
  disponible: boolean = true; //Control de disponibilidad
  priceMin: number = 0; //Precio min
  priceMax: number = 1000000; //Precio maximo
  qtyMin: number = 0; //Cantidad minima
  qtyMax: number = 2000; //Cantidad Maxima
  openCategory: boolean = false; //Control de menu de categorias
  //Categoria seleccionada
  categorySelect: any = {
    id: 0,
    name: '',
    sublevels: []
  };
  listCategory: any = []; //listado de categorias desde servicios
  listCategoryMenu: any = []; //Listado de categorias para el menu en base a la opciones seleccionadas


  constructor(private  queryService: QueryService, private helpersService: HelpersService) {
    this.loadProduct();

    this.queryService.getCategories(
      (res, err) => {
        if (err) {
          this.helpersService.errorMsg('Error en carga de categorias');
        }
        else {
          this.listCategory = res.categories;
          this.listCategoryMenu.push(_.cloneDeep(this.listCategory));
        }
      }
    );
  }

  /**
   * Carga de productos desde servicio y filtrado de los mismos (el modo correcto es desde una api pero en este caso se filtrara a nivel local)
   */
  loadProduct() {
    this.queryService.getProducts(
      (res, err) => {
        if (err) {
          this.helpersService.errorMsg('Error en carga de productos');
        }
        else {
          let keyOrder = 'price';
          let order = 'asc';
          switch (parseInt(this.orderBy)) {
            case 1: {
              keyOrder = 'price';
              order = 'asc';
              break;
            }
            case 2: {
              keyOrder = 'price';
              order = 'desc';
              break;
            }
            case 3: {
              keyOrder = 'quantity';
              order = 'desc';
              break;
            }
            case 4: {
              keyOrder = 'quantity';
              order = 'asc';
              break;
            }
            case 5: {
              keyOrder = 'available';
              order = 'desc';
              break;
            }
          }
          let list: any = _.orderBy(res.products, keyOrder, order);
          if (this.disponible) {
            list = _.remove(list,
              (n) => {
                return n.available;
              });
          }
          if (this.priceMin >= 0) {
            list = _.remove(list,
              (n) => {
                return (n.price >= this.priceMin);
              });
          }
          if (this.priceMax >= 0) {
            list = _.remove(list,
              (n) => {
                return (n.price <= this.priceMax);
              });
          }
          if (this.qtyMin >= 0) {
            list = _.remove(list,
              (n) => {
                return (n.quantity >= this.qtyMin);
              });
          }
          if (this.qtyMax >= 0) {
            list = _.remove(list,
              (n) => {
                return (n.quantity <= this.qtyMax);
              });
          }
          if (this.search !== '') {
            list = _.remove(list,
              (n) => {
                return n.name.toLowerCase().includes(this.search.toLowerCase());
              });
          }
          if (this.categorySelect.id > 0) {
            list = _.remove(list,
              (n) => {
                return (n.sublevel_id == this.categorySelect.id);
              });
          }
          this.listProducts = list;
        }
      }
    );
  }

  /**
   * Seleccion de categoria
   * @param item
   * @param listOrder
   */
  selectCategory(item, listOrder) {
    console.log(listOrder);
    if (item.sublevels) {
      let tam = this.listCategoryMenu.length - (listOrder + 1);
      this.listCategoryMenu = _.dropRight(this.listCategoryMenu, tam);
      this.listCategoryMenu.push(_.cloneDeep(item.sublevels));
    }
    else {
      this.categorySelect = item;
      console.log(this.categorySelect);
      this.openCategory = !this.openCategory;
      this.loadProduct();
    }
  }

  /**
   * Retornar a todos los productos y categorias
   */
  todos() {
    this.categorySelect = {
      id: 0,
      name: '',
      sublevels: []
    };
    this.listCategoryMenu.length = 0;
    this.listCategoryMenu.push(_.cloneDeep(this.listCategory));
    this.loadProduct();
  }
}
