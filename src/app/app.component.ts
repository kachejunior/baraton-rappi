import {Component} from '@angular/core';

@Component({
    selector: 'rp-baraton',
    template: '<rp-menu></rp-menu>' +
    '<router-outlet></router-outlet>' +
    '<toaster-container></toaster-container>',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app';
}
