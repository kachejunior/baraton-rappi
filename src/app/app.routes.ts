import {Routes} from "@angular/router";
import {HomeComponent} from './pages/home/home.component';
import {CartComponent} from './pages/cart/cart.component';

export const ROUTES: Routes = [
    {path: '', component: HomeComponent},
    {path: 'carrito', component: CartComponent}
];
