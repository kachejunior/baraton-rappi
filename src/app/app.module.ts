import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, PreloadAllModules} from '@angular/router';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {ToastrModule} from 'ngx-toastr';

import {ROUTES} from './app.routes';

import {QueryService, HelpersService} from './core';

import {AppComponent} from './app.component';
import {HomeComponent} from './pages/home/home.component';
import {CartComponent} from './pages/cart/cart.component';
import {MenuComponent, MiniComponent} from './shared';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CartComponent,
    MenuComponent,
    MiniComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    ToasterModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot(ROUTES, {useHash: false, preloadingStrategy: PreloadAllModules}),
  ],
  providers: [QueryService, HelpersService, ToasterService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
